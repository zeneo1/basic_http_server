defmodule Stack do
  use GenServer

  @stack __MODULE__

  def start_link(stack) do
    GenServer.start_link(@stack, stack, name: @stack)
  end


  def init(arg \\ []) do
    {:ok, arg}
  end

  def handle_cast({:push, elem}, stack) do
    {:noreply, [elem | stack]}
  end

  def handle_call(:pop, _from, [h | t]) do
    {:reply, h, t}
  end

  def handle_call(:pop, _from, []) do
    {:reply, :empty, []}
  end


  def handle_call(:get, _from, stack) do
    {:reply, stack, stack}
  end


  # api interfaces
  def push(elem) do
    GenServer.cast(@stack, {:push, elem})
  end

  def pop do
    GenServer.call(@stack, :pop)
  end

  def get do
    GenServer.call(@stack, :get)
  end

end
