defmodule Controller.Stack do

  alias Http.Response


  require Logger


  def push_request(request) do

    elem = request.path_vars |> Enum.at(0)
    Stack.push(elem)
    Response.respond(:OK, request.client)
  end

  def pop_request(request) do
    Response.respond(:OK, request.client, %{number: Stack.pop})
  end

  def get_all_request(request) do
    Response.respond(:OK, request.client, Stack.get)
  end

end
