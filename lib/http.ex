defmodule Http do
  use Application

  alias Http.Server

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_type, _args) do
    children = [
      {Task, fn -> Server.listen(4040) end},
      {Task.Supervisor, name: Http.Supervisor},
      {Stack, []}
    ]
    opts = [strategy: :one_for_one, restart: :permanant]
    Supervisor.start_link(children, opts)
  end

end
