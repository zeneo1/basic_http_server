defmodule Http.Server do

  alias Http.Handler

  def listen(port \\ 8080) do
    {:ok, socket} = :gen_tcp.listen(port, [:binary, reuseaddr: true, active: false])
    listener_loop(socket)
  end

  def listener_loop(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    :ok = :inet.setopts(client, packet: :http_bin)
    {:ok, pid} = Task.Supervisor.start_child(Http.Supervisor, fn -> Handler.recieve(client) end)
    :gen_tcp.controlling_process(client, pid)
    listener_loop(socket)
  end

end
