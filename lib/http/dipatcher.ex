defmodule Http.Dispatcher do



  @routes [
    {"/get", :GET, Controller.Stack, :get_all_request},
    {"/push/{elem}", :POST, Controller.Stack, :push_request},
    {"/pop", :POST, Controller.Stack, :pop_request}
  ]

  @structured_routes Enum.map(@routes, fn {path, method, module, func} ->
    ps = path
    |> String.split("/")
    |> Enum.reject(&("" === &1))
    |> Enum.map(fn p ->
      is_param = String.starts_with?(p, "{") && String.ends_with?(p, "}")
      if is_param do
        {p}
      else
        p
      end
    end)
    {ps, method, module, func}
  end)

  @content_type "application/json"
  @accept "application/json"
  @accept_all "*/*"
  @max_length 10240

  def dispatch(request) do
    matched = verify_route(request)
    valid_headers = verify_headers(request)

    if valid_headers do
      case matched do
        {:ok, {_, _, m, f}, path_vars} ->
          request = request
          |> Map.put(:path_vars, path_vars)
          apply(m, f, [request])
        {:error, _reason} ->
          :error
      end
    else
      Http.Response.respond(:BAD_REQUEST, request.client)
    end
  end

  def read_body(request) do
    length = (Keyword.has_key?(request.headers, :"Content-Type") && request.headers[:"Content-Type"])
    |> String.to_integer()

    if length > 0 do
      {something, packet} = :gen_tcp.recv(request.client, 0)
      IO.puts(something)
      Map.put(request, :body, Poison.decode!(packet))
    else
      request
    end
  end

  defp verify_route(request) do
    uri = URI.parse(request.path)
    ps = uri.path
    |> String.split("/")
    |> Enum.reject(&("" === &1))

    matched = @structured_routes
    |> Enum.filter(fn {path, _method, _module, _func} ->
      match_paths(ps, path)
    end)


    if length(matched) > 0 do
      matched = matched
      |> Enum.filter(fn {_, method, _module, _func} ->
        method == request.method
      end)
      if length(matched) > 0 do
        selected = Enum.at(matched, 0)
        {:ok, selected, path_vars(ps, elem(selected, 0), [])}
      else
        Http.Response.respond(:METHOD_NOT_ALLOWED, request.client)
        {:error, :reason}
      end
    else
      Http.Response.respond(:NOT_FOUND, request.client)
    end
  end


  def path_vars([h1 | t1], [{_} | t2], path_variables) do
    path_vars(t1, t2, path_variables ++ [h1])
  end

  def path_vars([_ | t1], [_ | t2], path_variables) do
    path_vars(t1, t2, path_variables)
  end

  def path_vars([], [], path_variables) do
    path_variables
  end

  def match_paths([_ | t1], [{_} | t2]) do
    match_paths(t1, t2)
  end

  def match_paths([h1 | t1], [h2 | t2]) when h1 === h2 do
    match_paths(t1, t2)
  end


  def match_paths([_ | _], [_ | _]) do
    false
  end

  def match_paths([], []) do
    true
  end

  def match_paths(_, _) do
    false
  end


  defp verify_headers(request) do
    valid_accept(request.headers)
    && valid_content_type(request.headers)
    && valid_content_length(request.headers)
  end

  defp valid_accept(headers) do
    !Keyword.has_key?(headers, :Accept) ||
    headers[:Accept] === @accept ||
    headers[:Accept] === @accept_all
  end

  defp valid_content_type(headers) do
    !Keyword.has_key?(headers, :"Content-Type")||
    headers[:"Content-Type"] === @content_type
  end

  defp valid_content_length(headers) do
    !Keyword.has_key?(headers, :"Content-Length")
    || headers[:"Content-Length"] |> String.to_integer < @max_length
  end

end
