defmodule Http.Response do


  def respond(:OK, socket) do
    message = "HTTP/1.0 200 Ok\n\n"
    :gen_tcp.send(socket, message)
    :gen_tcp.close(socket)
    exit(:normal)
  end


  def respond(:BAD_REQUEST, socket) do
    message = "HTTP/1.1 400 Bad Request\n\n"
    :gen_tcp.send(socket, message)
    :gen_tcp.close(socket)
    exit(:normal)
  end

  def respond(:METHOD_NOT_ALLOWED, socket) do
    message = "HTTP/1.1 405 Method Not Allowed\n\n"
    :gen_tcp.send(socket, message)
    :gen_tcp.close(socket)
    exit(:normal)
  end

  def respond(:NOT_FOUND, socket) do
    message = "HTTP/1.1 404 Not Found\n\n"
    :gen_tcp.send(socket, message)
    :gen_tcp.close(socket)
    exit(:normal)
  end


  def respond(:OK, socket, body) do
    {:ok, body} = Poison.encode(body)
    message = "HTTP/1.1 200 Ok\n" <>
    "Content-type: application/json\n\n" <>
    body
    :gen_tcp.send(socket, message)
    :gen_tcp.close(socket)
    exit(:normal)
  end

end
