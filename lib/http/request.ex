defmodule Http.Request do

  defstruct [
    path: nil,
    method: nil,
    headers: [],
    body: nil,
    client: nil,
    path_vars: []
  ]

end
