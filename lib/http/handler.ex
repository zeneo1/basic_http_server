defmodule Http.Handler do

  alias Http.Dispatcher

  def recieve(socket) do
    _recieve(socket, %Http.Request{})
  end

  defp _recieve(socket, request) do
    pkt = :gen_tcp.recv(socket, 0)
    IO.puts(inspect pkt)
    case pkt do
      {:ok, {:http_request, method, {:abs_path, path}, _}} ->
        request = request
        |> Map.put(:path, path)
        |> Map.put(:method, method)
        |> Map.put(:client, socket)
        _recieve(socket, request)
      {:ok, :http_eoh} ->
        :inet.setopts(socket, [{:packet, :raw}])
        Dispatcher.dispatch(request)
      {:ok, {:http_header, _, header, _, value}} ->
        request = request
        |> Map.update(:headers, [], fn headers ->
          [{header, value}] ++ headers
        end)
        _recieve(socket, request)
      _ ->
        exit(:closed)
    end
  end


end
